package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class UserRemoveCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        System.out.println("[USER REMOVE]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        getUserService().removeByLogin(login);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "User remove.";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-remove";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
