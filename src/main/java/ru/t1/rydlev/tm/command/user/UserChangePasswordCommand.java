package ru.t1.rydlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.rydlev.tm.enumerated.Role;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change password of current user.";
    }

    @NotNull
    @Override
    public String getName() {
        return "change-user-password";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
